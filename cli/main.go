package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	league "git.yequari.com/yequari/goleague/client" 
)

func writeMatchIds(matchIds []string) {
	f, err := os.Create("matches")
	if err != nil {
		log.Println("err")
	}
	w := bufio.NewWriter(f)
	for _, m := range matchIds {
		w.WriteString(fmt.Sprint(m))
	}
}

func getSummonerInfo(key string) {
	client := league.NewRiotClient(key, league.NA1, league.Americas)
	summoner, err := client.GetSummonerByName("yequari")
	if err != nil {
		log.Fatalln(err)
	}
	rankedLeague, err := client.GetLeagueBySummoner(summoner.Id)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println(rankedLeague)
	fmt.Println(summoner.Name)
	for _, l := range rankedLeague {
		fmt.Printf("%s %s %dLP %d%%\n", l.Tier, l.Rank, l.LeaguePoints, int(float64(l.Wins)/float64(l.Wins+l.Losses)*100))
		// fmt.Println(l.Tier, l.Rank, l.LeaguePoints)
	}
}

func readApiKey() string {
	f, err := os.Open("riot.key")
	if err != nil {
		log.Fatalf("could not open file riot.key")
	}
	r := bufio.NewReader(f)
	key, err := r.ReadString('\n')
	if err != nil {
		log.Fatalln("error reading key file")
	}
	return key
}

func main() {
	key := readApiKey()
	getSummonerInfo(key)
	client := league.NewRiotClient(key, league.NA1, league.Americas)
	summoner, err := client.GetSummonerByName("yequari")
	if err != nil {
		log.Fatalln(err)
	}
	// matchIds := client.GetMatchesByPuuid(summoner.Puuid, 0, 100)
	fmt.Printf("Summoner %s level %d\n", summoner.Name, summoner.SummonerLevel)
	// match := client.GetMatchById(matchIds[0])

	// matches := make([]league.Match, 0)
	// writeMatchIds(matchIds)
	// var seconds int64
	// for _, m := range matchIds {
	// 	match := client.GetMatchById(m)
	// 	seconds += match.Info.GameDuration
	// 	matches = append(matches, match)
	// }
	// remainingSecs := seconds % 60
	// minutes := (seconds % 3600) / 60
	// hours := seconds / 3600
	// fmt.Printf("Time played last %d games: %d seconds\n", len(matches), seconds)
	// fmt.Printf("%d hours %d minutes %d seconds\n", hours, minutes, remainingSecs)
}
