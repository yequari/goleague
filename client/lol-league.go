package client

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type LeagueEntry struct {
	LeagueId     string
	SummonerId   string
	SummonerName string
	QueueType    string
	Tier         string
	Rank         string
	LeaguePoints int
	Wins         int
	Losses       int
	HotStreak    bool
	Veteran      bool
	FreshBlood   bool
	Inactive     bool
	MiniSeries   MiniSeries
}

type MiniSeries struct {
	Losses   int
	Progress string
	Target   int
	Wins     int
}

const entriesPath = "lol/league/v4/entries"

func (l *LeagueEntry) Winrate() int {
    return int(float64(l.Wins)/float64(l.Wins+l.Losses)*100)
}

func (c *RiotClient) GetLeagueBySummoner(summonerId string) ([]LeagueEntry, error) {
	uri := c.constructPlatformUri(entriesPath+"/by-summoner", summonerId)
	leagueEntries := make([]LeagueEntry, 2)
	req, err := http.NewRequest(http.MethodGet, uri, nil)
	if err != nil {
		return nil, err
	}
	data, err := c.makeRequest(req)
	if err != nil {
		return nil, fmt.Errorf("api error: %w", err)
	}
	err = json.Unmarshal(data, &leagueEntries)
	if err != nil {
		return nil, fmt.Errorf("json error: %w", err)
	}
	return leagueEntries, nil
}
