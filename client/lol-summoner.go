package client

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type Summoner struct {
	AccountId     string
	ProfileIconId int
	RevisionDate  int64
	Name          string
	Id            string
	Puuid         string
	SummonerLevel int64
}

const summonerPath = "lol/summoner/v4/summoners"

func (c *RiotClient) GetSummonerByName(name string) (*Summoner, error) {

	uri := c.constructPlatformUri(summonerPath+"/by-name", name)
	req, err := http.NewRequest(http.MethodGet, uri, nil)
	if err != nil {
		return nil, err
	}
	data, err := c.makeRequest(req)
	if err != nil {
		return nil, fmt.Errorf("api error: %w", err)
	}
	var summoner Summoner
	err = json.Unmarshal(data, &summoner)
	if err != nil {
		return nil, fmt.Errorf("json error: %w", err)
	}
	return &summoner, nil
}
