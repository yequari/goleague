package client

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type Match struct {
	Metadata MatchMetadata `json:"metadata"`
	Info     MatchInfo     `json:"info"`
}

type MatchMetadata struct {
	DataVersion  string
	MatchId      string
	Participants []string
}

type MatchInfo struct {
	GameCreation       int64
	GameDuration       int64
	GameEndTimestamp   int64
	GameId             int64
	GameMode           string
	GameName           string
	GameStartTimestamp int64
	GameType           string
	GameVersion        string
	MapId              int
	Participants       []MatchParticipant
	PlatformId         string
	QueueId            int
	Teams              []interface{} // TODO Team struct
	TournamentCode     string
}

type MatchParticipant struct {
	Assists                        int
	BaronKills                     int
	BountyLevel                    int
	ChampExperience                int
	ChampLevel                     int
	ChampionId                     int
	ChampionName                   string
	ChampionTransform              int
	ConsumablesPurchased           int
	DamageDealtToBuildings         int
	DamageDealtToObjectives        int
	DamageDealtToTurrets           int
	DamageSelfMitigated            int
	Deaths                         int
	DetectorWardsPlaced            int
	DoubleKills                    int
	DragonKills                    int
	FirstBloodAssist               bool
	FirstBloodKill                 bool
	FirstTowerAssist               bool
	FirstTowerKill                 bool
	GameEndedInEarlySurrender      bool
	GameEndedInSurrender           bool
	GoldEarned                     int
	GoldSpent                      int
	IndividualPosition             string
	InhibitorKills                 int
	InhibitorTakedowns             int
	InhibitorsLost                 int
	Item0                          int
	Item1                          int
	Item2                          int
	Item3                          int
	Item4                          int
	Item5                          int
	Item6                          int
	ItemsPurchased                 int
	KillingSprees                  int
	Kills                          int
	Lane                           string
	LargestCriticalStrike          int
	LargestKillingSpree            int
	LargestMultiKill               int
	LongestTimeSpentLiving         int
	MagicDamageDealt               int
	MagicDamageDealtToChampions    int
	MagicDamageTaken               int
	NeutralMinionsKilled           int
	NexusKills                     int
	NexusTakedowns                 int
	NexusLost                      int
	ObjectivesStolen               int
	ObjectivesStolenAssists        int
	ParticipantId                  int
	PentaKills                     int
	Perks                          interface{} // TODO Perks struct
	PhysicalDamageDealt            int
	PhysicalDamageDealtToChampions int
	PhysicalDamageTaken            int
	ProfileIcon                    int
	Puuid                          string
	QuadraKills                    int
	RiotIdName                     string
	RiotIdTagline                  string
	Role                           string
	SightWardsBoughtInGame         int
	Spell1Casts                    int
	Spell2Casts                    int
	Spell3Casts                    int
	Spell4Casts                    int
	Summoner1Casts                 int
	Summoner1Id                    int
	Summoner2Casts                 int
	Summoner2Id                    int
	SummonerId                     string
	SummonerLevel                  int
	SummonerName                   string
	TeamEarlySurrendered           bool
	TeamId                         int
	TeamPosition                   string
	TimeCCingOthers                int
	TimePlayed                     int
	TotalDamageDealt               int
	TotalDamageDealtToChampions    int
	TotalDamageShieldedOnTeammates int
	TotalDamageTaken               int
	TotalHeal                      int
	TotalHealsOnTeammates          int
	TotalMinionsKilled             int
	TotalTimeCCDealt               int
	TotalTimeSpentDead             int
	TotalUnitsHealed               int
	TripleKills                    int
	TrueDamageDealt                int
	TrueDamageDealtToChampions     int
	TrueDamageTaken                int
	TurretKills                    int
	TurretTakedowns                int
	TurretsLost                    int
	UnrealKills                    int
	VisionScore                    int
	VisionWardsBoughtInGame        int
	WardsKilled                    int
	WardsPlaced                    int
	Win                            bool
}

const matchApi = "lol/match/v5/matches"

func (c *RiotClient) GetMatchById(id string) (*Match, error) {
	uri := c.constructRegionUri(matchApi, id)
	req, err := http.NewRequest(http.MethodGet, uri, nil)
	if err != nil {
		return nil, err
	}
	data, err := c.makeRequest(req)
	if err != nil {
		return nil, fmt.Errorf("api error: %w", err)
	}
	match := Match{}
	err = json.Unmarshal(data, &match)
	if err != nil {
		return nil, fmt.Errorf("json error: %w", err)
	}
	return &match, nil
}

func (c *RiotClient) GetMatchesByPuuid(puuid string, start int, count int) ([]string, error) {
	path := matchApi + "/by-puuid/" + puuid
	uri := c.constructRegionUri(path, "ids")
	req, err := http.NewRequest(http.MethodGet, uri, nil)
	if err != nil {
		return nil, err
	}
	q := req.URL.Query()
	q.Add("count", fmt.Sprint(count))
	q.Add("start", fmt.Sprint(start))
	req.URL.RawQuery = q.Encode()
	data, err := c.makeRequest(req)
	if err != nil {
		return nil, fmt.Errorf("api error: %w", err)
	}
	matchIds := make([]string, 0, count)
	err = json.Unmarshal(data, &matchIds)
	if err != nil {
		return nil, fmt.Errorf("json error: %w", err)
	}
	return matchIds, nil
}
