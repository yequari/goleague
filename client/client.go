package client

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"
)

type Platform string
type Region string

const (
	NA1 Platform = "https://na1.api.riotgames.com"
	// TODO: rest of platforms
)

const (
	Americas Region = "https://americas.api.riotgames.com"
	// TODO: rest of regions
)

type RiotClient struct {
	apikey   string
	Platform Platform
	Region   Region
}

func NewRiotClient(apikey string, platform Platform, region Region) *RiotClient {
	return &RiotClient{
		apikey:   apikey,
		Platform: platform,
		Region:   region,
	}
}

func (c *RiotClient) constructPlatformUri(path, endpoint string) string {
	return strings.Join([]string{string(c.Platform), path, endpoint}, "/")
}

func (c *RiotClient) constructRegionUri(path, endpoint string) string {
	return strings.Join([]string{string(c.Region), path, endpoint}, "/")
}

func (c *RiotClient) makeRequest(req *http.Request) ([]byte, error) {
	req.Header.Add("X-Riot-Token", c.apikey)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != 200 {
		return nil, errors.New(fmt.Sprintf("Response status %d", resp.StatusCode))
	}
	return body, nil
}
